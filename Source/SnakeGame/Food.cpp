// Fill out your copyright notice in the Description page of Project Settings.

#include "Food.h"
#include "SnakeBase.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood::RandomSpawnFood()
{
	float X = FMath::RandRange(-1300.f, 1300.f);
	float Y = FMath::RandRange(-1300.f, 1300.f);
	UE_LOG(LogTemp, Warning, TEXT("The float value is: %f"), X);
	UE_LOG(LogTemp, Warning, TEXT("The float value is: %f"), Y);

	FVector FoodLocation(X, Y, 0);
	FTransform FoodTransform(FoodLocation);

	AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, FoodTransform);
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			this->RandomSpawnFood();
			this->Destroy();
		}
	}
}